import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//modulo para uso de formularios:
import { FormsModule} from '@angular/forms';
//modulo para uso de peticiones AJAX
import {HttpClientModule} from '@angular/common/http';
import {AngularFileUploaderModule} from 'angular-file-uploader';

//librería para formatear fechas
import {MomentModule} from 'angular2-moment';

//librería para resaltar código
import {NgxHighlightJsModule} from '@nowzoo/ngx-highlight-js';

//importar routing
import {appRoutingProviders,routing} from './app.routing';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user.edit/user.edit.component';

import {PanelModule} from './panel/panel.module';
import { TopicsComponent } from './components/topics/topics.component';
import { TopicDetailComponent } from './components/topic-detail/topic-detail.component';

import {UserService} from './services/user.service';

import {UserGuard} from './services/user.guard';
import { UsersComponent } from './components/users/users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    UserEditComponent,
    TopicsComponent,
    TopicDetailComponent,
    UsersComponent,
    ProfileComponent,
    SearchComponent,   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpClientModule,
    AngularFileUploaderModule,
    MomentModule,
    PanelModule,  
    NgxHighlightJsModule.forRoot(), 
  ],
  providers: [appRoutingProviders,UserService,UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
