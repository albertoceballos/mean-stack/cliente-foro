import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
//modelo
import { Topic } from '../../../models/Topic';
//servicios
import { UserService } from '../../../services/user.service';
import { TopicService } from '../../../services/topic.service';

@Component({
  selector: 'panel-edit',
  templateUrl: '../add/add.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public pageTitle: string;
  public topic: Topic;
  public identity;
  public token;
  public status;
  public langSelect;
  public message;
  public btnLabel;

  constructor(private _userService: UserService,
    private _topicService: TopicService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {

    this.pageTitle = "Editar Tema";
    this.btnLabel='Editar';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.topic = new Topic('', '', '', '', '', '', this.identity._id, null);
    this.langSelect = [
      'JavaScript', 'TypeScript', 'C++', 'C#', 'Java', 'PHP', 'Python', 'Visual Basic', 'ASP', 'HTML', 'JQuery', 'Pascal', 'SQL', 'Ruby', 'Cobol'
    ];
    this.langSelect.sort();
    this.langSelect.push('Otro');
  }

  ngOnInit() {
    this.getTopic();
  }

  getTopic(){
    //recoger el parametro del id del topic de la ruta del navegador para luego pasarlo al llamar al servicio
    this._activatedRoute.params.subscribe(
      params=>{
        let topicId=params['id'];
       
        //llamar al servicio y hacer la petición Ajax

        this._topicService.getTopic(topicId).subscribe(
          response=>{
            if(response.topic){
              this.topic=response.topic;
            }
          },
          error=>{
            console.log(error);
          }
        );
      }
    );
  }

  onSubmit(form){
    //recoger el parametro del id del topic de la ruta del navegador para luego pasarlo al llamar al servicio
    this._activatedRoute.params.subscribe(
      params=>{
        let topicId=params['id'];

        this._topicService.updateTopic(topicId,this.token,this.topic).subscribe(
          response=>{
            if(response.topic){
              this.status='success';
              this.topic=response.topic;
              console.log(response.topic);
            }else{
              this.status='error';
            }
            this.message=response.message;
          },
          error=>{
            console.log(error);
            this.status='error';
          }
        );
      }
    );
  }

}
