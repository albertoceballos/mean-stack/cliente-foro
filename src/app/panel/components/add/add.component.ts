import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
//modelo
import { Topic } from '../../../models/Topic';
//servicios
import { UserService } from '../../../services/user.service';
import { TopicService } from '../../../services/topic.service';


@Component({
  selector: 'panel-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [UserService, TopicService],
})
export class AddComponent implements OnInit {

  public pageTitle: string;
  public topic: Topic;
  public identity;
  public token;
  public status;
  public langSelect;
  public message;
  public btnLabel;

  constructor(private _userService: UserService,
     private _topicService:TopicService,
     private _router: Router,
     private _activatedRoute: ActivatedRoute)
   {
    this.pageTitle = 'Crear nuevo Tema';
    this.btnLabel='Publicar';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.topic = new Topic('', '', '', '', '', '', this.identity._id, null);
    this.langSelect = [
      'JavaScript', 'TypeScript', 'C++', 'C#', 'Java', 'PHP', 'Python', 'Visual Basic', 'ASP', 'HTML', 'JQuery', 'Pascal', 'SQL', 'Ruby', 'Cobol'
    ];
    this.langSelect.sort();
    this.langSelect.push('Otro');

  }

  ngOnInit() {

  }

  onSubmit(form) {
    console.log(this.topic);
    this._topicService.addTopic(this.token,this.topic).subscribe(
      response=>{
        if(response.topic){
          this.status='success';
          this.message=response.message;
          console.log(response.topic);
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al publicar el tema';
      }
    );
  }
}
