import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//modulo para uso de formularios:
import { FormsModule} from '@angular/forms';
//modulo para uso de peticiones AJAX
import {HttpClientModule} from '@angular/common/http';
import {AngularFileUploaderModule} from 'angular-file-uploader';
//importar routing
import {panelRoutingProviders,routing} from './panel.routing';
//moment
import {MomentModule} from 'angular2-moment';

//componentes
import {MainComponent} from './components/main/main.component';
import {AddComponent} from './components/add/add.component';
import {EditComponent} from './components/edit/edit.component';
import {ListComponent} from './components/list/list.component';

//Servicios
import {UserService} from '../services/user.service';
//guard
import {UserGuard} from '../services/user.guard';

@NgModule({
    declarations:[
        MainComponent,
        AddComponent,
        EditComponent,
        ListComponent,
    ],
    imports:[
        BrowserModule,
        FormsModule,
        routing,
        HttpClientModule,
        AngularFileUploaderModule,
        MomentModule,
    ],
    providers:[panelRoutingProviders,UserService,UserGuard],

})
export class PanelModule{ }