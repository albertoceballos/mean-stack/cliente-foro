import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

//guar
import {UserGuard} from '../services/user.guard';

import { AddComponent } from './components/add/add.component';
import { EditComponent } from './components/edit/edit.component';
import { ListComponent } from './components/list/list.component';
import { MainComponent } from './components/main/main.component';

const panelRoutes:Routes=[
    {path:'panel',component:MainComponent,
        canActivate:[UserGuard],
        children:[
            {path:'',component:ListComponent},
            {path:'crear',component:AddComponent},
            {path:'listado',component:ListComponent},
            {path:'editar/:id',component:EditComponent},

        ],
    }

];

export const panelRoutingProviders:any[]=[];
export const routing:ModuleWithProviders=RouterModule.forRoot(panelRoutes);