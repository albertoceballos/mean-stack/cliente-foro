import { Component,OnInit,DoCheck } from '@angular/core';
import { Router, ActivatedRoute,Params} from '@angular/router';

//url global
import {GLOBAL} from './services/url';
//servicio
import {UserService} from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService],
})
export class AppComponent implements OnInit, DoCheck {
  public title = 'NgForo';
  public identity;
  public token;
  public url;
  public searchString;

  constructor(private _userService:UserService, private _router:Router, private _activatedRoute:ActivatedRoute){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.url=GLOBAL.url;
  }

  ngOnInit(){
   
    console.log(this.identity);
    console.log(this.token);
  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
  }

  
  logOut(){
    localStorage.clear();
    this.identity=null;
    this.token=null;
    this._router.navigate(['/login']);
  }

  onSubmit(){
    this._router.navigate(['buscar',this.searchString]);
  }
}
