import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute,Params } from '@angular/router';

//modelos
import { Topic } from '../../models/Topic';
import { User } from '../../models/User';

//servicios
import { TopicService } from '../../services/topic.service';
import { UserService } from '../../services/user.service';

import {GLOBAL} from '../../services/url';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers:[TopicService,UserService],
})
export class ProfileComponent implements OnInit {
  public pageTitle:string;
  public url:string;
  public status:string;
  public user:User;
  public topics:Array<Topic>;
  public token;
  public message;
  public num_temas;

  constructor(private _topicService:TopicService,private _userService:UserService, private _activatedRoute:ActivatedRoute) {
    this.pageTitle="Perfil de";
    this.url=GLOBAL.url;
    this.token=this._userService.getToken();
   }

  ngOnInit() {
    //extraer el id de usuario de la url
    this._activatedRoute.params.subscribe(
      params=>{
        let userId=params['id'];
        //lamar a los métodos psándo el id de usuario
        this.getUser(userId);
        this.getTopics(userId);
      }
    )
  }

  //sacar detalles de usuario
  getUser(userId){
    this._userService.getUser(userId).subscribe(
      response=>{
        if(response.user){
          this.user=response.user;        
        }
      },
      error=>{
        console.log(error);
      }
    );
  }

  getTopics(userId){
    this._topicService.getMyTopics(this.token,userId).subscribe(
      response=>{
        if(response.topics){
          console.log(response.topics);
          this.topics=response.topics;
          this.num_temas=this.topics.length;
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message=error.error.message;
      }
    );
  }
}
