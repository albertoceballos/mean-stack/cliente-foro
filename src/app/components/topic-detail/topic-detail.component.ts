import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';

//modelos
import {Topic} from '../../models/Topic';
import {Comment} from '../../models/Comment';
//servicios
import {TopicService} from '../../services/topic.service';
import {UserService} from '../../services/user.service';
import {CommentService} from '../../services/comment.service';

@Component({
  selector: 'app-topic-detail',
  templateUrl: './topic-detail.component.html',
  styleUrls: ['./topic-detail.component.css'],
  providers:[UserService,TopicService,CommentService],
})
export class TopicDetailComponent implements OnInit {
  
  public status;
  public message;
  public topic:Topic;
  public comment:Comment;
  public identity;
  public token;

  
  constructor(private _topicService:TopicService,
     private _userService:UserService,
     private _commentService:CommentService,
     private _router:Router,
     private _activatedRoute:ActivatedRoute,
     ) {
      this.identity=this._userService.getIdentity();
      this.token=this._userService.getToken();
      this.comment=new Comment('','','',this.identity._id);
   }

  ngOnInit() {
    
    this._activatedRoute.params.subscribe(
      params=>{
        var topicId=params['id'];
        this.getTopic(topicId);
      }
    );
    
  }

  //detalle del topic
  getTopic(topicId){
    this._topicService.getTopic(topicId).subscribe(
      response=>{
        if(response.topic){
          this.topic=response.topic;
        }else{
          this._router.navigate(['/inicio']);
        }
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    )
  }

  onSubmit(form){
    console.log(this.comment);

    this._commentService.add(this.token,this.topic._id,this.comment).subscribe(
      response=>{
        if(response.topic){
          console.log(response);
          this.status='success';
          form.reset();
          this.getTopic(this.topic._id);
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
  }

  deleteComment(commentId){
    this._commentService.remove(this.token,this.topic._id,commentId).subscribe(
      response=>{
        if(response.topic){
          console.log(response.message);
          this.getTopic(this.topic._id);
        }else{
          console.log(response.message);
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
}
