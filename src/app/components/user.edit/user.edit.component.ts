import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//url global
import { GLOBAL } from '../../services/url';
//modelos:
import { User } from '../../models/User';

//servicios:
import { UserService } from '../../services/user.service';

@Component({
  selector: 'user.edit',
  templateUrl: './user.edit.component.html',
  styleUrls: ['./user.edit.component.css'],
  providers: [UserService],
})
export class UserEditComponent implements OnInit {
  public title: string;
  public user: User;
  public identity;
  public token;
  public afuConfig;
  public url;
  public status:string;
  public message:string;


  constructor(private _userService: UserService) {
    this.title = "Ajustes de usuario";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = this.identity;
    this.url = GLOBAL.url;

    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png,.jpeg,.gif",
      maxSize: "20",
      uploadAPI: {
        url: this.url + "upload-avatar",
        headers: { "Authorization": this.token }
      },
      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: 'Select Files',
        resetBtn: 'Reset',
        uploadBtn: 'Upload',
        dragNDropBox: 'Drag N Drop',
        attachPinBtn: 'Sube tu foto...',
        afterUploadMsg_success: 'Foto subida con éxito!',
        afterUploadMsg_error: 'Error al subir la foto, inténtalo de nuevo !'
      }
    };
  }

  ngOnInit() {
  }

  avatarUpload(data){
    let data_obj=JSON.parse(data.response);
    this.user.image=data_obj.user.image;
    console.log(this.user);

  }

  onSubmit(){
    this._userService.upload(this.user).subscribe(
      response=>{
        if(response.user){
          this.status='success';
          this.message=response.message;
          localStorage.setItem('identity',JSON.stringify(this.user));
        }else{
          this.status='error';
          this.message=response.message;
        }      
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al actualizar el usuario';
      }
    );
  }
}
