import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

//modelos
import {Topic} from './../../models/Topic';

//servicios
import {TopicService} from '../../services/topic.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers:[TopicService],
})
export class SearchComponent implements OnInit {
  public title;
  public topics:Array<Topic>;
  public status;
  public message;
  public num_results;
  constructor(private _topicService:TopicService, private _router:Router, private _activatedroute:ActivatedRoute) {


   }

  ngOnInit() {
    //sacar el string de búsqueda que va por la url:
    this._activatedroute.params.subscribe(
      params=>{
        let searchString=params['search'];
        this.getSearch(searchString);
      }
    );
  }

  getSearch(searchString){
    this._topicService.search(searchString).subscribe(
      response=>{
        if(response.topics){
          this.topics=response.topics;
          console.log(this.topics);
          this.num_results=this.topics.length
          if(this.num_results<1){
            this.message="No hay resultados que coincidan con la búsqueda";
          }else{
            if(this.num_results==1){
              this.message=this.topics.length+" tema encontrado";
            }else{
              this.message=this.topics.length+" temas encontrados";
            }
          }
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
  }
}
