import { Component, OnInit } from '@angular/core';

//modelos
import {User} from '../../models/User';

//servicios
import {UserService} from '../../services/user.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService],
})
export class RegisterComponent implements OnInit {

  public title:string;
  public user:User;
  public status:string;
  public message:string;

  constructor(private _userService:UserService) {
    this.title="Registráte";
    this.user=new User('','','','','','','ROLE_USER');
   }

  ngOnInit() {
  }

  onSubmit(form){
    this._userService.register(this.user).subscribe(
      response=>{
        if(response.user && response.user._id){
          this.status=response.status;
          this.message=response.message;
          form.reset();
        }else{
          this.status='error';
          this.message="El registro no se ha completado";
        }
      },
      error=>{
        console.log(error);
      }
    );
  }

}
