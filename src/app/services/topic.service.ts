import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//url
import { GLOBAL } from './url';
//modelo
import { Topic } from '../models/Topic';

@Injectable({
    providedIn: 'root'
})
export class TopicService {
    public url;

    constructor(private _httpClient: HttpClient) {
        this.url = GLOBAL.url;
    }

    prueba() {
        console.log("hola desde el servicio TopicService");
    }

    //añadir topic
    addTopic(token, topic): Observable<any> {
        let params = JSON.stringify(topic);
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._httpClient.post(this.url + 'topic', params, { headers: headers });
    }

    //listado de mis topics
    getMyTopics(token, userId): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._httpClient.get(this.url + 'user-topics/' + userId, { headers: headers });


    }

    //sacar detalle del topic
    getTopic(topicId): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._httpClient.get(this.url + 'topic/' + topicId, { headers: headers });
    }

    //actualizar topic
    updateTopic(id, token, topic): Observable<any> {
        let params = JSON.stringify(topic)
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._httpClient.put(this.url + 'topic/' + id, params, { headers: headers });
    }

    //borrar topic
    deleteTopic(id, token):Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._httpClient.delete(this.url + 'topic/' + id, { headers: headers });
    }

    //Todos los topics:
    getTopics(page=1):Observable<any>{
        let headers=new HttpHeaders().set('Content-Type','application/json');

        return this._httpClient.get(this.url+'topics/'+page,{headers:headers});

    }

    //busqueda
    search(searchString):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._httpClient.get(this.url+'search/'+searchString,{headers:headers});

    }

}