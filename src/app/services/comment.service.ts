import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//url
import { GLOBAL } from './url';
//modelo
import { Topic } from '../models/Topic';
import { Comment } from '../models/Comment';

@Injectable({
    providedIn: 'root'
})
export class CommentService {
    public url;

    constructor(private _httpClient: HttpClient) {
        this.url = GLOBAL.url;
    }

    //añadir comentario
    add(token,topicId,comment):Observable<any>{
        let params=JSON.stringify(comment);
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

        return this._httpClient.post(this.url+'comment/topic/'+topicId,params,{headers:headers});
    }

    //quitar comentario
    remove(token,topicId,commentId):Observable<any>{
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

        return this._httpClient.delete(this.url+'comment/'+topicId+'/'+commentId,{headers:headers});
    }


}